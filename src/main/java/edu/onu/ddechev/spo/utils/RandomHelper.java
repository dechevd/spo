package edu.onu.ddechev.spo.utils;

public class RandomHelper {

    private RandomHelper() {
    }

    private final static java.util.Random RANDOM = new java.util.Random();

    private static Number getGaussian(Number standardDeviation, Number medianValue) {
        return RANDOM.nextGaussian() * standardDeviation.doubleValue() + medianValue.doubleValue();
    }

    private static Number getGaussian(Number standardDeviation) {
        return RandomHelper.getGaussian(0.0F, standardDeviation);
    }

    public static Float gaussian(Float standardDeviation, Float medianValue) {
        return getGaussian(standardDeviation, medianValue).floatValue();
    }

    public static Float gaussian(Float standardDeviation) {
        return getGaussian(standardDeviation).floatValue();
    }

    public static Double gaussian(Double standardDeviation, Double medianValue) {
        return getGaussian(standardDeviation, medianValue).doubleValue();
    }

    public static Double gaussian(Double standardDeviation) {
        return getGaussian(standardDeviation).doubleValue();
    }

    public static Integer gaussian(Integer standardDeviation, Integer medianValue) {
        return getGaussian(standardDeviation, medianValue).intValue();
    }

    public static Integer gaussian(Integer standardDeviation) {
        return getGaussian(standardDeviation).intValue();
    }

    public static Long gaussian(Long standardDeviation, Long medianValue) {
        return getGaussian(standardDeviation, medianValue).longValue();
    }

    public static Long gaussian(Long standardDeviation) {
        return getGaussian(standardDeviation).longValue();
    }

    public static Integer uniform(Integer min, Integer max) {
        return RANDOM.nextInt(max - min + 1) + min;
    }

    public static Long uniform(Long min, Long max) {
        return uniform(min.intValue(), max.intValue()).longValue();
    }

    public static Double uniform(Double min, Double max) {
        return uniform(min.intValue(), max.intValue() - 1) + uniform();
    }

    public static Float uniform(Float min, Float max) {
        return uniform(min.doubleValue(), max.doubleValue()).floatValue();
    }

    public static Double uniform() {
        return RANDOM.nextDouble();
    }

    public static Boolean chance(Double probability) {
        return RANDOM.nextDouble() < probability;
    }
}
