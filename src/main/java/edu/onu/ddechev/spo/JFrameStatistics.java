package edu.onu.ddechev.spo;

import edu.onu.ddechev.spo.model.ModelStatistics;

/**
 *
 * @author denis
 */
public class JFrameStatistics extends javax.swing.JFrame {

    private final ModelStatistics modelStatistics;

    /**
     * Creates new form NewJFrame
     *
     * @param modelStatistics
     */
    public JFrameStatistics(ModelStatistics modelStatistics) {
        this.modelStatistics = modelStatistics;
        initComponents();
        updateStatistics();
    }

    public final void updateStatistics() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("количество поступивших в ВС заданий: ")
                .append(modelStatistics.getTotalProcesses()).append("\n");
        stringBuilder.append("количество завершившихся заданий: ")
                .append(modelStatistics.getFinishedProcesses()).append("\n");
        stringBuilder.append("среднее время ожидания для завершившихся заданий: ")
                .append(String.format("%.4f", modelStatistics.getAverageWaitingTime())).append("\n");
        stringBuilder.append("среднее время оборота для завершившихся заданий: ")
                .append(String.format("%.4f", modelStatistics.getAverageCirculatingTime())).append("\n");
        stringBuilder.append("максимальная длина очереди к процессору: ")
                .append(modelStatistics.getMaximumQueueSize()).append("\n");
        stringBuilder.append("количество тактов простоя процессора: ")
                .append(modelStatistics.getIdleTicks()).append("\n");
        jTextAreaStatistics.setText(stringBuilder.toString());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaStatistics = new javax.swing.JTextArea();

        setTitle("Статистика");

        jTextAreaStatistics.setEditable(false);
        jTextAreaStatistics.setColumns(20);
        jTextAreaStatistics.setRows(5);
        jScrollPane1.setViewportView(jTextAreaStatistics);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaStatistics;
    // End of variables declaration//GEN-END:variables
}
