package edu.onu.ddechev.spo.queue;

import java.util.NoSuchElementException;

public class PriorityQueueBinaryHeap<T extends Comparable<T>> extends PriorityQueue<T> {

    @Override
    public boolean add(T e) {
        super.add(e);
        // помещает элемент в конец очереди
        size++;
        int index = size;
        array[index] = e;

        bubbleUp();
        return true;
    }

    @Override
    public boolean offer(T e) {
        return add(e);
    }

    /**
     * Возвращает и удаляет вершину очереди. 
     * Бросает исключение если очередь пуста
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    @Override
    public T remove() {
        T result = element();

        array[1] = array[size];
        array[size] = null;
        size--;

        bubbleDown();

        return result;
    }

    /**
     * Возвращает и удаляет вершину очереди. 
     * Возвращает null если очередь пуста
     *
     * @return the head of this queue, or {@code null} if this queue is empty
     */
    @Override
    public T poll() {
        T result = peek();

        if (result != null) {
            array[1] = array[size];
            array[size] = null;
            size--;

            bubbleDown();
        }

        return result;
    }

    /**
     * Возвращает без удаления вершину очереди. 
     * Бросает исключение если очередь пуста
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    @Override
    public T element() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }
        return array[1];
    }

    /**
     * Возвращает без удаления вершину очереди. 
     * Возвращает null если очередь пуста
     *
     * @return the head of this queue, or {@code null} if this queue is empty
     */
    @Override
    public T peek() {
        if (this.isEmpty()) {
            return null;
        }
        return array[1];
    }

    /**
     * Опускает вниз элемент с основания дерева до его верного положения.
     */
    private void bubbleDown() {
        int index = 1;

        // bubble down
        while (hasLeftChild(index)) {
            // which of my children is smaller?
            int smallerChild = leftIndex(index);

            // bubble with the smaller child, if I have a smaller child
            if (hasRightChild(index)
                    && array[leftIndex(index)].compareTo(array[rightIndex(index)]) > 0) {
                smallerChild = rightIndex(index);
            }

            if (array[index].compareTo(array[smallerChild]) > 0) {
                swap(index, smallerChild);
            } else {
                // otherwise, get outta here!
                break;
            }

            // make sure to update loop counter/index of where last el is put
            index = smallerChild;
        }
    }

    /**
     * Поднимает вверх новый элемент конца очереди до его верного положения.
     *
     */
    private void bubbleUp() {
        int index = this.size;

        while (hasParent(index)
                && (parent(index).compareTo(array[index]) > 0)) {
            // parent/child are out of order; swap them
            swap(index, parentIndex(index));
            index = parentIndex(index);
        }
    }

    private boolean hasParent(int i) {
        return i > 1;
    }

    private int leftIndex(int i) {
        return i * 2;
    }

    private int rightIndex(int i) {
        return i * 2 + 1;
    }

    private boolean hasLeftChild(int i) {
        return leftIndex(i) <= size;
    }

    private boolean hasRightChild(int i) {
        return rightIndex(i) <= size;
    }

    private T parent(int i) {
        return array[parentIndex(i)];
    }

    private int parentIndex(int i) {
        return i / 2;
    }

    private void swap(int index1, int index2) {
        T tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;
    }

}
