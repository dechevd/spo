package edu.onu.ddechev.spo.queue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public abstract class PriorityQueue<T extends Comparable<T>> implements Queue<T> {

    private static final int DEFAULT_CAPACITY = 10;
    protected T[] array;
    protected int size;

    @SuppressWarnings("unchecked")
    public PriorityQueue() {
        // Java doesn't allow construction of arrays of placeholder data types 
        array = (T[]) new Comparable[DEFAULT_CAPACITY];
        size = 0;
    }

    protected final T[] resize() {
        return Arrays.copyOf(array, array.length * 2);
    }

    @Override
    public boolean add(T e) {
        if (e == null) {
            throw new NullPointerException("Binary heap doesn't accept null elements");
        }
        // grow array if needed
        if (size >= array.length - 1) {
            array = this.resize();
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return c.stream().map(this::add).allMatch(x -> x);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int pos = 1;

            @Override
            public boolean hasNext() {
                return size >= pos;
            }

            @Override
            public T next() {
                return (T) array[pos++];
            }
        };
    }

    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        array = (T[]) new Comparable[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        boolean isFirstElement = true;
        for (T e : array) {
            if (e != null) {
                if (!isFirstElement) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(e.toString());
                isFirstElement = false;
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

}
