package edu.onu.ddechev.spo.model;

import edu.onu.ddechev.spo.model.scheduler.SJFBurstTime;
import edu.onu.ddechev.spo.model.scheduler.ProcessorScheduler;
import edu.onu.ddechev.spo.model.scheduler.ResourceScheduler;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import edu.onu.ddechev.spo.queue.PriorityQueueBinaryHeap;
import edu.onu.ddechev.spo.utils.RandomHelper;
import java.util.IntSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Model implements TimeDiscreteElement {

    private final PriorityQueue<Process> processorQueue = new PriorityQueueBinaryHeap<>();
    private final Process.Generator processGenerator = new Process.Generator();
    private final Processor processor;
    private ProcessorScheduler processorScheduler;
    private final Timer timer = new Timer();
    private Double intensityThreshold;
    private Integer burstTimeMax;
    private Integer burstTimeMin;
    private final ModelStatistics modelStatistics = new ModelStatistics();
    private final List<ResourceScheduler> resourceSchedulers;
    private final List<Resource> resources;
    private final List<Queue<Process>> resourceQueues;

    public Model(Double intensityThreshold, Integer burstTimeMin, Integer burstTimeMax, Integer resourceCount) {
        this(intensityThreshold, burstTimeMin, burstTimeMax, resourceCount, new ProcessorNOP(), new SJFBurstTime.Builder());
    }

    public Model(Double intensityThreshold, Integer burstTimeMin, Integer burstTimeMax, Integer resourceCount,
            Processor processor, ProcessorScheduler.Builder processorSchedulerBuilder) {
        checkParameters(intensityThreshold, burstTimeMin, burstTimeMax, resourceCount);
        this.intensityThreshold = intensityThreshold;
        this.burstTimeMax = burstTimeMax;
        this.burstTimeMin = burstTimeMin;
        this.processor = processor;
        resources = IntStream.range(0, resourceCount)
                .mapToObj(i -> new ResourceNOP(String.valueOf(i))).collect(Collectors.toList());
        resourceQueues = IntStream.range(0, resourceCount)
                .mapToObj(i -> new LinkedList<Process>()).collect(Collectors.toList());
        resourceSchedulers = IntStream.range(0, resourceCount)
                .mapToObj(i -> new ResourceScheduler(resources.get(i), resourceQueues.get(i), processorQueue))
                .collect(Collectors.toList());
        this.processorScheduler = processorSchedulerBuilder.build(processor, processorQueue, resourceQueues);
    }

    private void checkParameters(Double intensityThreshold, Integer burstTimeMin, Integer burstTimeMax, Integer resourceCount) {
        if (intensityThreshold > 1.0 && intensityThreshold < 0) {
            throw new IllegalArgumentException("Порог интенсивности поступления процессов должен быть в диапазоне [0.0, 1.0]");
        }
        if (burstTimeMax < 0) {
            throw new IllegalArgumentException("Mаксимальная величина интервала обслуживания должна быть больше 0");
        }
        if (burstTimeMin < 0) {
            throw new IllegalArgumentException("Минимальная величина интервала обслуживания должна быть больше 0");
        }
        if (burstTimeMin > burstTimeMax) {
            throw new IllegalArgumentException("Минимальная величина интервала обслуживания должна быть меньше максимальной");
        }
        if (resourceCount < 0) {
            throw new IllegalArgumentException("Количество ресурсов должно быть больше 0");
        }
    }

    @Override
    public void tick(Integer t) {
        Integer time = timer.increment();
        if (RandomHelper.chance(intensityThreshold)) {
            createProcess(time);
        }
        processorScheduler.tick(time);
        processor.tick(time);
        resourceSchedulers.forEach(rs -> rs.tick(time));
        resources.forEach(r -> r.tick(time));
        processorQueue.forEach(Process::incrementWaitingTime);
        resourceQueues.forEach(queue -> queue.forEach(Process::incrementWaitingTime));
        updateStatistics();
    }

    protected Process createProcess(Integer time) {
        Integer burstTime = RandomHelper.uniform(burstTimeMin, burstTimeMax);
        return createProcess(time, burstTime);
    }

    protected Process createProcess(Integer time, Integer burstTime) {
        Process process = processGenerator.create(time, burstTime, processorScheduler::getPriority);
        process.setQueuingTime(time);
        processorQueue.add(process);
        return process;
    }

    public void clear() {
        timer.reset();
        processor.reset();
        processorQueue.clear();
        processorScheduler.clear();
        processGenerator.reset();
        resourceQueues.forEach(Queue::clear);
        resources.forEach(Resource::reset);
    }

    public PriorityQueue<Process> getProcessorQueue() {
        return processorQueue;
    }

    public Processor getProcessor() {
        return processor;
    }

    public ProcessorScheduler getProcessorScheduler() {
        return processorScheduler;
    }

    public void setProcessorScheduler(ProcessorScheduler processorScheduler) {
        this.processorScheduler = processorScheduler;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setIntensityThreshold(Double intensityThreshold) {
        this.intensityThreshold = intensityThreshold;
    }

    public void setBurstTimeMax(Integer burstTimeMax) {
        this.burstTimeMax = burstTimeMax;
    }

    public void setBurstTimeMin(Integer burstTimeMin) {
        this.burstTimeMin = burstTimeMin;
    }

    public List<Queue<Process>> getResourceQueues() {
        return resourceQueues;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public ModelStatistics getStatistics() {
        return modelStatistics;
    }

    private void updateStatistics() {
        Integer totalProcesses = processorQueue.size() + processorScheduler.getFinishedProcesses().size() 
                + resourceQueues.stream().collect(Collectors.summingInt(queue -> queue.size()))
                + resources.stream().collect(Collectors.summingInt(resource -> resource.isIdle() ? 0 : 1));
        if (processor.isIdle()) {
            modelStatistics.idleTicks++;
        } else {
            totalProcesses++;
        }
        modelStatistics.totalProcesses = totalProcesses;
        modelStatistics.finishedProcesses = processorScheduler.getFinishedProcesses().size();
        IntSummaryStatistics waitingTimeStatistics = processorScheduler.getFinishedProcesses().stream()
                .mapToInt(process -> process.getWaitingTime()).summaryStatistics();
        modelStatistics.averageWaitingTime = waitingTimeStatistics.getAverage();
        IntSummaryStatistics circulatingTimeStatistics = processorScheduler.getFinishedProcesses().stream()
                .mapToInt(process -> process.getWaitingTime() + process.getProcessingTime()).summaryStatistics();
        modelStatistics.averageCirculatingTime = circulatingTimeStatistics.getAverage();
        if (modelStatistics.maximumQueueSize < processorQueue.size()) {
            modelStatistics.maximumQueueSize = processorQueue.size();
        }
    }

}
