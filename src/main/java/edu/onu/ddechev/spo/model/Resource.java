package edu.onu.ddechev.spo.model;

import java.util.Optional;

public abstract class Resource implements TimeDiscreteElement, Named {

    protected final String name;

    protected Optional<Process> processOptional = Optional.empty();

    protected Resource(String name) {
        this.name = name;
    }

    public Boolean isIdle() {
        return !processOptional.isPresent();
    }

    public Process getProcess() {
        return processOptional.get();
    }

    public void setProcess(Process process) {
        processOptional = Optional.ofNullable(process);
    }

    public abstract Integer getBusyInterval();

    @Override
    public void tick(Integer time) {
        if (!isIdle()) {
            getProcess().incrementWaitingTime();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void reset() {
        setProcess(null);
    }

}
