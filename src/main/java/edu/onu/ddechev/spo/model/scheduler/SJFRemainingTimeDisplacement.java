package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.List;
import java.util.Queue;

/**
 * SJF с вытеснением по оставшемуся времени [3]
 *
 * @author denis
 */
public class SJFRemainingTimeDisplacement extends SJFRemainingTime {

    public static class Builder extends ProcessorScheduler.Builder<SJFRemainingTimeDisplacement> {

        @Override
        public SJFRemainingTimeDisplacement build(Processor processor, PriorityQueue<Process> priorityQueue,
                List<Queue<Process>> resourcesQueues) {
            return new SJFRemainingTimeDisplacement(processor, priorityQueue, resourcesQueues);
        }

    }

    protected SJFRemainingTimeDisplacement(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }

    @Override
    protected Boolean isDisplacementNeed() {
        Process displacementProcess = priorityQueue.peek();
        return (displacementProcess != null) && (!processor.isIdle())
                && (displacementProcess.getPriority() < processor.getProcess().getPriority());
    }

    @Override
    public String getName() {
        return "SJF ВытОстВремя";
    }
}
