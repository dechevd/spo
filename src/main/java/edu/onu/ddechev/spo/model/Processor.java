package edu.onu.ddechev.spo.model;

public abstract class Processor extends Resource {
    
    public Processor(String name) {
        super(name);
    }
    
    @Override
    public void setProcess(Process process) {
        super.setProcess(process);
        processOptional.ifPresent(p -> p.setStatus(Process.ProcessStatus.ACTIVE));
    }
    
    @Override
    public void tick(Integer time) {
        if (!isIdle()) {
            getProcess().incrementProcessingTime();
        }
    }
    
    @Override
    public Integer getBusyInterval() {
        return getProcess().getBurstTime() - getProcess().getProcessingTime();
    }
    
}
