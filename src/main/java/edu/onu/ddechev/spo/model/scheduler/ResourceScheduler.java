package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Resource;
import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.TimeDiscreteElement;
import edu.onu.ddechev.spo.utils.RandomHelper;
import java.util.Collection;
import java.util.Queue;

public class ResourceScheduler implements TimeDiscreteElement {

    protected final Resource resource;
    protected final Queue<Process> resourceQueue;
    protected final Collection<Process> finishedProcesses;

    public ResourceScheduler(Resource resource, Queue<Process> resourceQueue, Collection<Process> finishedProcesses) {
        this.resource = resource;
        this.resourceQueue = resourceQueue;
        this.finishedProcesses = finishedProcesses;
    }

    @Override
    public void tick(Integer time) {
        if (resource.isIdle()) {
            resource.setProcess(resourceQueue.poll());
        } else {
            if (isProcessFinished()) {
                addBurstTime(resource.getProcess());
                resource.getProcess().setQueuingTime(time);
                moveCurrentProcess(Process.ProcessStatus.READY, finishedProcesses);
            }
        }
    }

    // Метод освобождает ресурс, и переносит процесс в коллекцию с указанным статусом
    protected void moveCurrentProcess(Process.ProcessStatus processStatus,
            Collection<Process> collectionToMove) {
        Process process = resource.getProcess();
        if (process != null) {
            process.setStatus(processStatus);
            collectionToMove.add(process);
            resource.setProcess(null);
        }
    }

    protected Boolean isProcessFinished() {
        return resource.getBusyInterval() == 0;
    }

    private void addBurstTime(Process process) {
        Integer burstTime = process.getBurstTime();
        process.setBurstTime(burstTime + RandomHelper.uniform(1, burstTime));
    }

}
