package edu.onu.ddechev.spo.model;

public interface Named {

    public String getName();
}
