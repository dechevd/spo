package edu.onu.ddechev.spo.model;

import edu.onu.ddechev.spo.utils.RandomHelper;

public class ResourceNOP extends Resource {

    private Integer busyInterval;

    public ResourceNOP(String name) {
        super(name);
        defineBusyInterval();
    }

    @Override
    public void setProcess(Process process) {
        super.setProcess(process);
        if (!isIdle()) {
            defineBusyInterval();
        }
    }

    @Override
    public void tick(Integer time) {
        super.tick(time);
        if (!isIdle()) {
            busyInterval--;
        }
    }

    @Override
    public Integer getBusyInterval() {
        return busyInterval;
    }

    private void defineBusyInterval() {
        busyInterval = RandomHelper.uniform(1, 10);
    }

}
