package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import edu.onu.ddechev.spo.utils.RandomHelper;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

/**
 * SJF без вытеснения
 *
 * @author denis
 */
public abstract class SJF extends ProcessorScheduler {

    protected SJF(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }

    @Override
    public void tick(Integer time) {
        if (rearrangementNeed()) {
            if (!processor.isIdle()) {
                if (isProcessFinished()) {
                    Optional<Queue<Process>> maybeResourceQueue = getMaybeResourceQueue();
                    if (maybeResourceQueue.isPresent()) {
                        moveCurrentProcess(Process.ProcessStatus.WAITING, maybeResourceQueue.get());
                    } else {
                        moveCurrentProcess(Process.ProcessStatus.TERMINATED, finishedProcesses);
                    }
                }
                if (isDisplacementNeed()) {
                    displaceCurrentProcess();
                }
            }
            processor.setProcess(priorityQueue.poll());
        }
    }

    protected void displaceCurrentProcess() {
        Process process = processor.getProcess();
        if (process != null) {
            process.setStatus(Process.ProcessStatus.WAITING);
            priorityQueue.add(process);
            processor.reset();
        }
    }

    protected Boolean rearrangementNeed() {
        return processor.isIdle() || isProcessFinished() || isDisplacementNeed();
    }

    protected Boolean isDisplacementNeed() {
        return false;
    }

    private Optional<Queue<Process>> getMaybeResourceQueue() {
        Integer resourceChance = RandomHelper.uniform(0, resourcesQueues.size());
        if (resourceChance == 0) {
            return Optional.empty();
        } else {
            return Optional.of(resourcesQueues.get(resourceChance - 1));
        }
    }

}
