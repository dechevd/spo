package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.List;
import java.util.Map;
import java.util.Queue;

//TODO
public class SJFEstimationBurstTime extends SJFBurstTime {

    public static class Builder extends ProcessorScheduler.Builder<SJFEstimationBurstTime> {

        @Override
        public SJFEstimationBurstTime build(Processor processor, PriorityQueue<Process> priorityQueue,
                List<Queue<Process>> resourcesQueues) {
            return new SJFEstimationBurstTime(processor, priorityQueue, resourcesQueues);
        }

    }

    private Map<Process, List<Integer>> processingHistory;

    public SJFEstimationBurstTime(Processor processor, PriorityQueue<Process> priorityQueue, List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }



}
