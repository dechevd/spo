package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.List;
import java.util.Queue;

/**
 * SJF без вытеснения c приоритетом по оставшемуся времени
 *
 * @author denis
 */
public class SJFRemainingTime extends SJF {

    public static class Builder extends ProcessorScheduler.Builder<SJFRemainingTime> {

        @Override
        public SJFRemainingTime build(Processor processor, PriorityQueue<Process> priorityQueue,
                List<Queue<Process>> resourcesQueues) {
            return new SJFRemainingTime(processor, priorityQueue, resourcesQueues);
        }

    }

    protected SJFRemainingTime(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }

    @Override
    public Integer getPriority(Process process) {
        // приоритет по оставшемуся времени
        return process.getBurstTime() - process.getProcessingTime();
    }

    @Override
    public String getName() {
        return "SJF ОстВремя";
    }
}
