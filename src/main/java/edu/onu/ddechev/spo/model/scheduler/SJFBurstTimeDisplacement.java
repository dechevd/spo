package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.List;
import java.util.Queue;

/**
 * SJF с вытеснением по исходному интервалу [2]
 *
 * @author denis
 */
public class SJFBurstTimeDisplacement extends SJFBurstTime {

    public static class Builder extends ProcessorScheduler.Builder<SJFBurstTimeDisplacement> {

        @Override
        public SJFBurstTimeDisplacement build(Processor processor, PriorityQueue<Process> priorityQueue,
                List<Queue<Process>> resourcesQueues) {
            return new SJFBurstTimeDisplacement(processor, priorityQueue, resourcesQueues);
        }

    }

    protected SJFBurstTimeDisplacement(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }

    @Override
    protected Boolean isDisplacementNeed() {
        Process displacementProcess = priorityQueue.peek();
        return (displacementProcess != null) && (!processor.isIdle())
                && (displacementProcess.getPriority() < processor.getProcess().getPriority());
    }

    @Override
    public String getName() {
        return "SJF ВытИсхИнт";
    }
}
