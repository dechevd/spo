package edu.onu.ddechev.spo.model;

public class Timer implements TimeDiscreteElement {

    private Integer time = 0;

    public Integer get() {
        return time;
    }

    public Integer increment() {
        return ++time;
    }

    void reset() {
        time = 0;
    }

    @Override
    public void tick(Integer time) {
        increment();
    }
}
