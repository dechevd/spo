package edu.onu.ddechev.spo.model;

import java.util.function.Function;

public abstract class Process implements Comparable<Process>, Named {

    public static enum ProcessStatus {
        READY, ACTIVE, WAITING, TERMINATED;
    }

    public static class Generator {

        private Integer index = 1;

        public Process create(Integer creationTime, Integer burstTime,
                Function<Process, Integer> getPriority) {
            return new Process(index++, creationTime, burstTime) {
                @Override
                public Integer getPriority() {
                    return getPriority.apply(this);
                }
            };
        }

        public void reset() {
            index = 1;
        }
    }

    private final Integer id;
    private ProcessStatus status = ProcessStatus.READY;
    private Integer burstTime;
    private final Integer creationTime;
    private Integer waitingTime = 0;
    private Integer queuingTime;
    private Integer processingTime = 0;

    private Process(Integer id, Integer creationTime, Integer burstTime) {
        this.id = id;
        this.creationTime = creationTime;
        this.burstTime = burstTime;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCreationTime() {
        return creationTime;
    }

    public ProcessStatus getStatus() {
        return status;
    }

    public void setStatus(ProcessStatus status) {
        this.status = status;
    }

    public abstract Integer getPriority();

    public Integer getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Integer burstTime) {
        this.burstTime = burstTime;
    }

    public Integer getWaitingTime() {
        return waitingTime;
    }

    public void incrementWaitingTime() {
        this.waitingTime++;
    }

    public Integer getQueuingTime() {
        return queuingTime;
    }

    public void setQueuingTime(Integer queuingTime) {
        this.queuingTime = queuingTime;
    }

    public Integer getProcessingTime() {
        return processingTime;
    }

    public void incrementProcessingTime() {
        this.processingTime++;
    }

    @Override
    public int compareTo(Process process) {
        return Integer.compare(getPriority(), process.getPriority());
    }

    @Override
    public String getName() {
        return String.format("PID%d(%d)", id, creationTime);
    }

    @Override
    public String toString() {
        return "Process{" + "id=" + id + ", status=" + status + ", priority=" + getPriority() + ", burstTime=" + burstTime + ", creationTime=" + creationTime + ", waitingTime=" + waitingTime + ", queuingTime=" + queuingTime + ", processingTime=" + processingTime + '}';
    }

}
