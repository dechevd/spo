package edu.onu.ddechev.spo.model;

public class ModelStatistics {

    Integer totalProcesses;
    Integer finishedProcesses;
    Double averageWaitingTime;
    Double averageCirculatingTime;
    Integer maximumQueueSize = 0;
    Integer idleTicks = 0;

    public Integer getTotalProcesses() {
        return totalProcesses;
    }

    public Integer getFinishedProcesses() {
        return finishedProcesses;
    }

    public Double getAverageWaitingTime() {
        return averageWaitingTime;
    }

    public Double getAverageCirculatingTime() {
        return averageCirculatingTime;
    }

    public Integer getMaximumQueueSize() {
        return maximumQueueSize;
    }

    public Integer getIdleTicks() {
        return idleTicks;
    }

    @Override
    public String toString() {
        return "ModelStatistic{" + "totalProcesses=" + totalProcesses + ", finishedProcesses=" + finishedProcesses + ", averageWaitingTime=" + averageWaitingTime + ", averageCirculatingTime=" + averageCirculatingTime + ", maximumQueueSize=" + maximumQueueSize + ", idleTicks=" + idleTicks + '}';
    }

}
