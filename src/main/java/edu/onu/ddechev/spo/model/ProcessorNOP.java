package edu.onu.ddechev.spo.model;

public class ProcessorNOP extends Processor {

    public ProcessorNOP() {
        super("CPU");
    }

}
