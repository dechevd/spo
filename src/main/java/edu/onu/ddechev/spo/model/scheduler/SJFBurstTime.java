package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.List;
import java.util.Queue;

/**
 * SJF без вытеснения c приоритетом по исходному интервалу [1]
 *
 * @author denis
 */
public class SJFBurstTime extends SJF {

    public static class Builder extends ProcessorScheduler.Builder<SJFBurstTime> {

        @Override
        public SJFBurstTime build(Processor processor, PriorityQueue<Process> priorityQueue, 
                List<Queue<Process>> resourcesQueues) {
            return new SJFBurstTime(processor, priorityQueue, resourcesQueues);
        }

    }

    protected SJFBurstTime(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, resourcesQueues);
    }

    @Override
    public Integer getPriority(Process process) {
        // приоритет по времени выполнения
        return process.getBurstTime();
    }

    @Override
    public String getName() {
        return "SJF ИсхИнт";
    }

}
