package edu.onu.ddechev.spo.model.scheduler;

import edu.onu.ddechev.spo.model.Model;
import edu.onu.ddechev.spo.model.Named;
import edu.onu.ddechev.spo.model.Process;
import edu.onu.ddechev.spo.model.Processor;
import edu.onu.ddechev.spo.queue.PriorityQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public abstract class ProcessorScheduler extends ResourceScheduler implements Named {

    public static abstract class Builder<T extends ProcessorScheduler> {

        public abstract T build(Processor processor, PriorityQueue<Process> priorityQueue,
                List<Queue<Process>> resourcesQueues);

        public T build(Model model) {
            return this.build(model.getProcessor(), model.getProcessorQueue(), model.getResourceQueues());
        }
    }

    protected final Processor processor;
    protected final PriorityQueue<Process> priorityQueue;
    protected final List<Queue<Process>> resourcesQueues;

    protected ProcessorScheduler(Processor processor, PriorityQueue<Process> priorityQueue,
            List<Queue<Process>> resourcesQueues) {
        super(processor, priorityQueue, new ArrayList<>());
        this.processor = processor;
        this.priorityQueue = priorityQueue;
        this.resourcesQueues = resourcesQueues;
    }

    public abstract Integer getPriority(Process process);

    public List<Process> getFinishedProcesses() {
        // вернем новый список, чтоб никто не мог изменить существующий
        return new ArrayList<>(finishedProcesses);
    }

    public void clear() {
        finishedProcesses.clear();
    }

}
