package edu.onu.ddechev.spo.model;

public interface TimeDiscreteElement {

    void tick(Integer time);
}
