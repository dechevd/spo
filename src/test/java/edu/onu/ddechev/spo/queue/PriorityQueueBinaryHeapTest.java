package edu.onu.ddechev.spo.queue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import org.junit.Test;

public class PriorityQueueBinaryHeapTest {

    @Test
    public void testAdd() {
        Integer expected = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.add(expected);
        assertEquals(expected, priorityQueue.peek());
        assertEquals(1, priorityQueue.size());
        assertFalse(priorityQueue.isEmpty());
    }

    @Test
    public void testOrder() {
        Integer expected = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.add(15);
        priorityQueue.add(expected);
        priorityQueue.add(25);
        assertEquals(expected, priorityQueue.peek());
        assertEquals(3, priorityQueue.size());
        assertFalse(priorityQueue.isEmpty());
    }

    @Test
    public void testAddAll() {
        Integer expected = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.addAll(Arrays.asList(15, expected, 25));
        assertEquals(expected, priorityQueue.peek());
        assertEquals(3, priorityQueue.size());
        assertFalse(priorityQueue.isEmpty());
    }

    @Test
    public void testDelete() {
        Integer expected = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.add(expected);
        assertEquals(expected, priorityQueue.remove());
        assertTrue(priorityQueue.isEmpty());
        assertEquals(0, priorityQueue.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void testDeleteEmpty() {
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.remove();
        assertTrue(priorityQueue.isEmpty());
        assertEquals(0, priorityQueue.size());
    }

    @Test
    public void testPoll() {
        Integer expected = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.add(expected);
        assertEquals(expected, priorityQueue.poll());
        assertTrue(priorityQueue.isEmpty());
        assertEquals(0, priorityQueue.size());
    }

    @Test
    public void testPollEmpty() {
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        assertNull(priorityQueue.poll());
        assertTrue(priorityQueue.isEmpty());
        assertEquals(0, priorityQueue.size());
    }

    @Test
    public void testToString() {
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        priorityQueue.addAll(Arrays.asList(1, 2, 3));
        assertEquals("[1, 2, 3]", priorityQueue.toString());
    }

    @Test
    public void testMassiveCollection() {
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        Random random = new Random();
        int size = 10000;
        Stream.generate(() -> random.nextInt()).limit(size)
                .collect(Collectors.toCollection(() -> priorityQueue));
        assertEquals(size, priorityQueue.size());
    }
    
    @Test
    public void testIterator() {
        int size = 10;
        PriorityQueue<Integer> priorityQueue = new PriorityQueueBinaryHeap<>();
        IntStream.range(0, size).mapToObj(x -> x)
                .collect(Collectors.toCollection(() -> priorityQueue));
        Iterator<Integer> iterator = priorityQueue.iterator();
        int index = 1;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), priorityQueue.toArray()[index++]);
        }
        assertEquals(size, priorityQueue.size());
        assertEquals(priorityQueue.size(), index - 1);
    }
    
}
