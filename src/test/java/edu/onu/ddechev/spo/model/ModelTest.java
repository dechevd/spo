package edu.onu.ddechev.spo.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ModelTest {

    @Test
    public void processCanBeExecuted() {
        Integer ticks = 10;
        Model model = new Model(0.0, ticks, ticks, 0);
        Process process = model.createProcess(model.getTimer().get());
        assertEquals("очередь содержит процесс", 1, model.getProcessorQueue().size());
        assertEquals("интервал обслуживания должен быть верным", ticks, process.getBurstTime());
        assertEquals("время работы равно 0", 0, process.getProcessingTime().intValue());
        assertEquals("процесс в состоянии READY", Process.ProcessStatus.READY, process.getStatus());
        // запуск модели на 10 тактов
        for (int i = 0; i < ticks; i++) {
            model.tick(null);
            assertEquals("процесс в состоянии ACTIVE", Process.ProcessStatus.ACTIVE, process.getStatus());
        }
        assertEquals("должно пройти 10 тактов", ticks, model.getTimer().get());
        assertTrue("очередь процессов пуста", model.getProcessorQueue().isEmpty());
        assertEquals("процесс выполнялся 10 тактов", ticks, process.getProcessingTime());
        model.tick(null);
        assertTrue("процесс есть в списке завершенных процессов",
                model.getProcessorScheduler().getFinishedProcesses().contains(process));
        assertEquals("процесс в состоянии DONE", Process.ProcessStatus.TERMINATED, process.getStatus());
        assertTrue("процессор простаивает", model.getProcessor().isIdle());
    }

    @Test
    public void waitingProcessIncreaseWaitingTime() {
        Integer ticks1 = 5;
        Integer ticks2 = 10;
        Model model = new Model(0.0, 0, 0, 0);
        Process process1 = model.createProcess(model.getTimer().get(), ticks1);
        Process process2 = model.createProcess(model.getTimer().get(), ticks2);
        assertEquals("очередь содержит процессы", 2, model.getProcessorQueue().size());
        for (Integer i = 0; i <= ticks1 + ticks2; i++) {
            model.tick(null);
        }
        assertEquals("время ожидания процесса1 верное", 0, process1.getWaitingTime().intValue());
        assertEquals("время выполнения процесса1 верное", ticks1, process1.getProcessingTime());
        assertEquals("время ожидания процесса2 верное", ticks1, process2.getWaitingTime());
        assertEquals("время выполнения процесса2 верное", ticks2, process2.getProcessingTime());
    }
    
        @Test
    public void nextProcessOnSameTickWithPrevious() {
        Integer ticks1 = 1;
        Integer ticks2 = 1;
        Model model = new Model(0.0, 0, 0, 0);
        Process process1 = model.createProcess(model.getTimer().get(), ticks1);
        Process process2 = model.createProcess(model.getTimer().get(), ticks2);
        assertEquals("очередь содержит процессы", 2, model.getProcessorQueue().size());
        for (Integer i = 0; i <= ticks1 + ticks2; i++) {
            model.tick(null);
        }
        assertEquals("процесс в состоянии DONE", Process.ProcessStatus.TERMINATED, process1.getStatus());
        assertEquals("процесс в состоянии DONE", Process.ProcessStatus.TERMINATED, process2.getStatus());
        assertTrue("процессор простаивает", model.getProcessor().isIdle());
    }

    @Test
    public void modelIdle() {
        Integer ticks = 1000;
        Model model = new Model(0.0, 0, 0, 0);
        for (int i = 0; i < ticks; i++) {
            model.tick(null);
            assertTrue("очередь процессов пуста", model.getProcessorQueue().isEmpty());
            assertTrue("список завершенных процессов пуст", model.getProcessorScheduler().getFinishedProcesses().isEmpty());
            assertTrue("процессор простаивает", model.getProcessor().isIdle());
        }
    }
}
